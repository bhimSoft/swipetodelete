package com.bhim.swipetodelete;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 24/03/2017.
 */

public abstract class SwipeAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final LayoutInflater mLayoutInflater;
    public Context mContext;
    private int mCurrentSwipePosition = -1;
    private final View.OnClickListener mDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            onDeleteClick(position);
        }
    };
    private final View.OnClickListener mCloseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onCloseClick();
        }
    };
    private final View.OnClickListener mAEditClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            onEditClick(position);
        }
    };

    public SwipeAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FrameLayout view = (FrameLayout) mLayoutInflater.inflate(R.layout.swipe_item, parent, false);
        return onCreateSwipeViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        View actionItemContainer = holder.itemView.findViewById(R.id.action_item_container);
        onBindSwipeViewHolder((VH) holder, position);

        View edit = holder.itemView.findViewById(R.id.swipe_edit);
        View delete = holder.itemView.findViewById(R.id.swipe_delete);
        View close = holder.itemView.findViewById(R.id.swipe_close);
        edit.setTag(position);
        delete.setTag(position);
        close.setTag(position);
        edit.setOnClickListener(mAEditClickListener);
        delete.setOnClickListener(mDeleteClickListener);
        close.setOnClickListener(mCloseClickListener);
        View child = ((FrameLayout) holder.itemView).getChildAt(1);
        if (mCurrentSwipePosition == position) {
            child.setAlpha(0f);
            actionItemContainer.setVisibility(View.VISIBLE);
            child.setClickable(false);
        } else {
            actionItemContainer.setVisibility(View.GONE);
            child.setAlpha(1f);
            child.setClickable(true);
        }
    }

    public void onSwipe(int position) {
        mCurrentSwipePosition = position;
        notifyDataSetChanged();
    }

    private void onEditClick(int position) {
        onEdit(position);
        reset();
    }

    private void onDeleteClick(int position) {
        onDelete(position);
        reset();
    }

    private void onCloseClick() {
        notifyDataSetChanged();
        reset();
    }

    private void reset() {
        mCurrentSwipePosition = -1;
    }

    public abstract void onEdit(int position);

    public abstract void onDelete(int position);

    public abstract void onBindSwipeViewHolder(VH holder, int position);

    public abstract VH onCreateSwipeViewHolder(ViewGroup parent, int viewType);
}
