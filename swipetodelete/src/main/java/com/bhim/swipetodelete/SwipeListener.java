package com.bhim.swipetodelete;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 25/03/2017.
 */

public class SwipeListener<AD extends SwipeAdapter> extends ItemTouchHelper.SimpleCallback {
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private AD mAd;

    public SwipeListener(AD ad, int swipeDirs) {
        super(0, swipeDirs);
        mAd = ad;
        mPaint.setColor(ContextCompat.getColor(mAd.mContext, R.color.colorAccent));
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        mAd.onSwipe(position);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        Bitmap icon;
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            View itemView = viewHolder.itemView;
            float height = (float) itemView.getBottom() - (float) itemView.getTop();
            float width = height / 3;

            if (dX > 0) {

                RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                c.drawRect(background, mPaint);
                icon = BitmapFactory.decodeResource(mAd.mContext.getResources(), R.drawable.ic_action_close_swipe);
                RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, mPaint);
            } else {
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, mPaint);
                icon = BitmapFactory.decodeResource(mAd.mContext.getResources(), R.drawable.ic_action_close_swipe);
                RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, mPaint);
            }
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    public void setColor(int colorResourceId) {
        mPaint.setColor(ContextCompat.getColor(mAd.mContext, colorResourceId));
    }
}
