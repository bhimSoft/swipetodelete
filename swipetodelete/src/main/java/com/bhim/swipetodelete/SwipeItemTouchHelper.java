package com.bhim.swipetodelete;

import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 25/03/2017.
 */

public class SwipeItemTouchHelper extends ItemTouchHelper {

    public SwipeItemTouchHelper(SwipeAdapter swipeAdapter) {
        super(new SwipeListener<SwipeAdapter>(swipeAdapter, ItemTouchHelper.LEFT));
    }

    public SwipeItemTouchHelper(SwipeAdapter swipeAdapter, int direction) {
        super(new SwipeListener<SwipeAdapter>(swipeAdapter, direction));
    }
}
