package com.bhim.swpetodeletdemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bhim.swipetodelete.SwipeAdapter;
import com.bhim.swipetodelete.SwipeItemTouchHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

        SwipeItemTouchHelper swipeItemTouchHelper = new SwipeItemTouchHelper(adapter, ItemTouchHelper.LEFT);
        swipeItemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private static class RecyclerViewAdapter extends SwipeAdapter<DemoHolder> {
        private final List<String> mData = new ArrayList<>();

        public RecyclerViewAdapter(Context context) {
            super(context);
            for (int i = 0; i < 30; i++) {
                mData.add("Item -" + (i + 1));
            }
        }

        @Override
        public void onEdit(int position) {

        }

        @Override
        public void onDelete(int position) {
            mData.remove(position);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        @Override
        public void onBindSwipeViewHolder(DemoHolder holder, final int position) {
            holder.title.setText(mData.get(position));
            holder.parentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Clicked " + position, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public DemoHolder onCreateSwipeViewHolder(ViewGroup parent, int viewType) {
            return new DemoHolder(mLayoutInflater.inflate(R.layout.recyclerview_item, parent, true));
        }
    }

    private static class DemoHolder extends RecyclerView.ViewHolder {
        TextView title;
        View parentView;

        public DemoHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            parentView = itemView.findViewById(R.id.parent_view);
        }
    }
}
